/**
    Generate rosbags from human readable datafiles in COLD database
    Janindu
*/

#include <iostream>
#include <fstream>
#include <vector>

#include "odom_scan.hpp"

const std::string SCAN_FILE_PATH = "odom_scans/scans.tdf";
const std::string ODOM_FILE_PATH = "odom_scans/odom.tdf";
const std::string BAG_FILE_PATH = "odom_scans/odom_scans_tf.bag";
const std::string SCAN_TOPIC = "/scan";
const std::string ODOM_TOPIC = "/odom";

int main(int argc, char** argv) {
    std::string parentPath;
    std::string scanFileName;
    std::string odomFileName;
    std::string bagFileName;

    std::string scanTopic;
    std::string odomTopic;

    std::ifstream scanFileStream;
    std::ifstream odomFileStream;

    std::vector<std::string> scanData;
    std::vector<std::string> odomData;

    std::string tmpString;

    // Need the parent folder path of the  human readable files
    if (argc < 2) {
        std::cerr << "Error executing genRosbag" << std::endl;
        std::cerr << "./genRosbag <path_to_parent_folder_of_human_readable_files> OPTIONAL[<scan_topic> <odom_topic>]" << std::endl;
        return 1;
    } else {
        parentPath = argv[1];
        std::cout << "Reading data from " << parentPath  << " ... " << std::endl << std::endl;
    }

    if (argc > 2) {
        std::cout << "Default topic names overwritten" << std::endl;
        scanTopic = argv[2];
        odomTopic = argv[3];
        std::cout << "scan topic : " << scanTopic << std::endl;
        std::cout << "odom topic : " << odomTopic << std::endl << std::endl;
    } else {
        scanTopic = SCAN_TOPIC;
        odomTopic = ODOM_TOPIC;
    }

    scanFileName = parentPath + "/" + SCAN_FILE_PATH;
    odomFileName = parentPath + "/" + ODOM_FILE_PATH;
    bagFileName = parentPath + "/" + BAG_FILE_PATH;

    scanFileStream.open(scanFileName.c_str(), std::ifstream::in);
    odomFileStream.open(odomFileName.c_str(), std::ifstream::in);

    if (scanFileStream.is_open()) {
        while (!scanFileStream.eof()) {
            std::getline(scanFileStream, tmpString);
            scanData.push_back(tmpString);
            // std::cout <<    scanData.back() << std::endl << std::endl;
        }
        std::cout << scanData.size() << " scans read from file" << std::endl << std::endl;
    } else {
        std::cerr << "Error opening " << scanFileName << std::endl;
        return 1;
    }

    if (odomFileStream.is_open()) {
         while (!odomFileStream.eof()) {
            std::getline(odomFileStream, tmpString);
            odomData.push_back(tmpString);
            // std::cout <<    scanData.back() << std::endl << std::endl;
        }
        std::cout << odomData.size() << " odometry read from file" << std::endl << std::endl;
    } else {
        std::cerr << "Error opening " << odomFileName << std::endl;
        return 1;
    }
    
    scanFileStream.close();
    odomFileStream.close();

    OdomScan odomScan(scanData, odomData);
    odomScan.generateRosbags(bagFileName, scanTopic, odomTopic);

    std::cout << "Successful!" << std::endl;

    return 0;
}
