/**
    ODOM_SCAN implementation
*/

#include <vector>
#include <iostream>
#include <rosbag/bag.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_msgs/TFMessage.h>
#include <geometry_msgs/PoseStamped.h>
#include <chrono>
#include <thread>

#include "odom_scan.hpp"

const std::string LASER_FRAME = "laser";
const std::string ODOM_FRAME = "odom";
const std::string BASE_LINK_FRAME = "baselink";

OdomScan::OdomScan(std::vector<std::string> scanStr, std::vector<std::string> odomStr) {
    this->scanStr = scanStr;
    this->odomStr = odomStr;
}

void OdomScan::generateRosbags(std::string bagFileName, std::string scanTopic, std::string odomTopic) {
    this->bagFileName = bagFileName;
    this->scanTopic = scanTopic;
    this->odomTopic = odomTopic;
    this->tfTopic = "/tf";

// First, parse the strings and create an ordered set of scans-odoms
// Then write to bag
    this->parseStrings();
    // this->validateData();
    this->writeRosbag();
}

void OdomScan::parseStrings() {
    // The odometry and scan file formats are available in appendix B : https://www.pronobis.pro/publications/ullah2007cold.pdf
    std::vector<std::string> parsedStr;
    
    // Since declaring variables inside a loop is inefficient, temp msg is declared here
    // Need to be careful to overwrite everything from the previous temp msg
    sensor_msgs::LaserScan scanMsg;
    nav_msgs::Odometry odomMsg;
    tf2::Quaternion quat_tf;

    // These values are constant for the sensor so will be parsed only once for efficiency
    double angle_min;
    double angle_max;
    double angle_increment;
    double range_max;
    int numReadings;

    boost::split(parsedStr, this->scanStr[0], boost::is_any_of(" "));

    numReadings = boost::lexical_cast<int>(parsedStr[2]);
    angle_increment = boost::lexical_cast<double>(parsedStr[12]);
    angle_min = boost::lexical_cast<double>(parsedStr[13]);
    angle_max = angle_min + (angle_increment*(numReadings-1));
    range_max = boost::lexical_cast<double>(parsedStr[14]);

    std::cout << "LaserScan constants : " << std::endl;
    std::cout << "num_readings : " << numReadings << std::endl;
    std::cout << "angle_increment : " << angle_increment << std::endl;
    std::cout << "angle_min : " << angle_min << std::endl;
    std::cout << "angle_max : " << angle_max << std::endl << std::endl;

    // Clear the current odom and scan vectors
    this->scanMsgVect.clear();
    this->odomMsgVect.clear();


    for (int i=0; i<this->scanStr.size(); i++) {
        boost::split(parsedStr, this->scanStr[i], boost::is_any_of(" "));

        if (parsedStr.size() != numReadings+16) {
            std::cout << "String " << i + 1 << " ignored (out of " << scanStr.size() << ")!" << std::endl;
            std::cout << "String split into " << parsedStr.size() << std::endl;
            std::cout << "Expected " << numReadings + 16 << std::endl;
            std::cout << "Possibly new line at the end of file" << std::endl << std::endl;
            continue;
        }

        scanMsg.header.seq = i+1;
        scanMsg.header.stamp.sec = boost::lexical_cast<int>(parsedStr[3]);
        scanMsg.header.stamp.nsec = boost::lexical_cast<int>(parsedStr[4]);
        scanMsg.header.frame_id = LASER_FRAME;

        scanMsg.angle_min = angle_min;
        scanMsg.angle_max = angle_max;
        scanMsg.angle_increment = angle_increment;
        scanMsg.range_max = range_max;

        for (int j=0; j<numReadings; j++) {
            scanMsg.ranges.push_back(boost::lexical_cast<float>(parsedStr[j+16]));
        }
        
        this->scanMsgVect.push_back(scanMsg);

        scanMsg.ranges.clear();
    }

    for (int i=0; i<this->odomStr.size(); i++) {
        boost::split(parsedStr, this->odomStr[i], boost::is_any_of(" "));

        if (parsedStr.size() != 14) {
            std::cout << "String " << i + 1 << " ignored (out of " << odomStr.size() << ")!" << std::endl;
            std::cout << "String split into " << parsedStr.size() << std::endl;
            std::cout << "Expected 14" << std::endl;
            std::cout << "Possibly new line at the end of file" << std::endl << std::endl;
            continue;   
        }

        odomMsg.header.seq = i+1;
        odomMsg.header.stamp.sec = boost::lexical_cast<int>(parsedStr[3]);
        odomMsg.header.stamp.nsec = boost::lexical_cast<int>(parsedStr[4]);
        odomMsg.header.frame_id = ODOM_FRAME;
        odomMsg.child_frame_id = BASE_LINK_FRAME;

        odomMsg.pose.pose.position.x = boost::lexical_cast<double>(parsedStr[8]);
        odomMsg.pose.pose.position.y = boost::lexical_cast<double>(parsedStr[9]);
        odomMsg.pose.pose.position.z = boost::lexical_cast<double>(parsedStr[10]);

        quat_tf.setRPY(
            boost::lexical_cast<double>(parsedStr[13]),
            boost::lexical_cast<double>(parsedStr[12]),
            boost::lexical_cast<double>(parsedStr[11])
        );

        quat_tf.normalize();

        odomMsg.pose.pose.orientation = tf2::toMsg(quat_tf);

        this->odomMsgVect.push_back(odomMsg);
    }
    
    std::cout << "Scan and Odom strings parsed" << std::endl;
    std::cout << this->scanStr.size() << " strings vs " << this->scanMsgVect.size() << " scan msgs" << std::endl;
    std::cout << this->odomStr.size() << " strings vs " << this->odomMsgVect.size() << " odom msgs" << std::endl << std::endl;
}

void OdomScan::validateData() {
    for (int i=0; i<this->scanMsgVect.size(); i++) {
        std::cout << "i : " << i << std::endl;
        std::cout << "seq : " << this->scanMsgVect[i].header.seq << std::endl;
        std::cout << "sec : " << this->scanMsgVect[i].header.stamp.sec << std::endl;
        std::cout << "nsec: " << this->scanMsgVect[i].header.stamp.nsec << std::endl << std::endl;
        sleep(0.01);
    }

    for (int i=0; i<this->odomMsgVect.size(); i++) {
        std::cout << "i : " << i << std::endl;
        std::cout << "seq : " << this->odomMsgVect[i].header.seq << std::endl;
        std::cout << "sec : " << this->odomMsgVect[i].header.stamp.sec << std::endl;
        std::cout << "nsec: " << this->odomMsgVect[i].header.stamp.nsec << std::endl << std::endl;
        sleep(0.01);
    }
}

void OdomScan::writeRosbag() {
    rosbag::Bag bag;
    sensor_msgs::LaserScan currentScan;
    nav_msgs::Odometry currentOdom;
    tf2_msgs::TFMessage currentTF;

    int scanIndex = 0;
    int odomIndex = 0;
    bool listsEmpty = false;
    
    bag.open(this->bagFileName, rosbag::bagmode::Write);

    currentScan = this->scanMsgVect[scanIndex];
    currentOdom = this->odomMsgVect[odomIndex];

    while (!listsEmpty) {
        if (compareStamps(currentScan, currentOdom)) {
            bag.write(
                this->scanTopic,
                ros::Time(currentScan.header.stamp.sec, currentScan.header.stamp.nsec),
                currentScan);
            scanIndex++;
            if (scanIndex < this->scanMsgVect.size()) {
                currentScan = this->scanMsgVect[scanIndex];
            } else {
                listsEmpty = true;
            }
        } else {
            bag.write(
                this->odomTopic,
                ros::Time(currentOdom.header.stamp.sec, currentOdom.header.stamp.nsec),
                currentOdom);
            currentTF = this->getTFMessage(currentOdom);
            bag.write(
                this->tfTopic,
                ros::Time(currentOdom.header.stamp.sec, currentOdom.header.stamp.nsec),
                currentTF);
            odomIndex++;
            if (odomIndex < this->odomMsgVect.size()) {
                currentOdom = this->odomMsgVect[odomIndex];
            } else {
                listsEmpty = true;
            }
        }
    }

    if (scanIndex != this->scanMsgVect.size() && odomIndex != this->odomMsgVect.size()) {
        std::cerr << "Error writing to rosbag" << std::endl;
        std::cerr << "Scan Index : " << scanIndex << " vector size : " << this->scanMsgVect.size() << std::endl;
        std::cerr << "Odom Index : " << odomIndex << " vector size : " << this->odomMsgVect.size() << std::endl;
        return;
    } else if (scanIndex != this->scanMsgVect.size()) {
        for (int i=scanIndex; i < this->scanMsgVect.size(); i++) {
            currentScan = this->scanMsgVect[i];
            bag.write(
                this->scanTopic,
                ros::Time(currentScan.header.stamp.sec, currentScan.header.stamp.nsec),
                currentScan);
        }
    } else if (odomIndex != this->odomMsgVect.size()) {
        for (int i=odomIndex; i < this->odomMsgVect.size(); i++) {
            currentOdom = this->odomMsgVect[i];
            bag.write(
                this->odomTopic,
                ros::Time(currentOdom.header.stamp.sec, currentOdom.header.stamp.nsec),
                currentOdom);
            currentTF = this->getTFMessage(currentOdom);
            bag.write(
                this->tfTopic,
                ros::Time(currentOdom.header.stamp.sec, currentOdom.header.stamp.nsec),
                currentTF);
        }
    }

    bag.close();

    std::cout << "Successfully written rosbag to : " << this->bagFileName << std::endl << std::endl;
}

// Returns true if laser scan has an earlier timestamp
// Returns false otherwise
bool OdomScan::compareStamps(sensor_msgs::LaserScan currScan, nav_msgs::Odometry currOdom) {
    double scanS = currScan.header.stamp.sec;
    double scanNs = currScan.header.stamp.nsec;

    double odomS = currScan.header.stamp.sec;
    double odomNs = currScan.header.stamp.nsec;

    return (scanS*1e9) + scanNs < (odomS*1e9) + odomNs;
}

tf2_msgs::TFMessage OdomScan::getTFMessage(nav_msgs::Odometry odomMsg) {
    tf2_msgs::TFMessage tfMsg;
    geometry_msgs::TransformStamped tfStampedMsg;

    tfStampedMsg.header = odomMsg.header;
    tfStampedMsg.child_frame_id = BASE_LINK_FRAME;

    tfStampedMsg.transform.translation.x = odomMsg.pose.pose.position.x;
    tfStampedMsg.transform.translation.y = odomMsg.pose.pose.position.y;
    tfStampedMsg.transform.translation.z = odomMsg.pose.pose.position.z;
    
    tfStampedMsg.transform.rotation = odomMsg.pose.pose.orientation;

    tfMsg.transforms.push_back(tfStampedMsg);

    return tfMsg;
}