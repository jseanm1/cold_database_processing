/**
    Class definition of ODOM_SCAN
*/

#ifndef ODOM_SCAN
#define ODOM_SCAN

#include <vector>
#include <rosbag/bag.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <tf2_msgs/TFMessage.h>

class OdomScan {
    private :
        std::vector<std::string> scanStr;
        std::vector<std::string> odomStr;
        std::string bagFileName;
        std::string scanTopic;
        std::string odomTopic;
        std::string tfTopic;
        std::vector<sensor_msgs::LaserScan> scanMsgVect;
        std::vector<nav_msgs::Odometry> odomMsgVect;

        void parseStrings(void);
        void validateData();
        void writeRosbag(void);
        bool compareStamps(sensor_msgs::LaserScan, nav_msgs::Odometry);
        tf2_msgs::TFMessage getTFMessage(nav_msgs::Odometry);

    public :
        OdomScan(std::vector<std::string>, std::vector<std::string>);

        void generateRosbags(std::string, std::string, std::string);
};

#endif
