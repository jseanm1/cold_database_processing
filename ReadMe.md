Currently, generating a rosbag with sensor_msgs/LaserScan, nav_msgs/Odometry and tf is implemented.

After building, run
	./genRosbag <path_to_parent_folder_of_human_readable_files> OPTIONAL[<scan_topic> <odom_topic>]

Example 1:
	./genRosbag ~/Downloads/seq1_cloudy1
Example 2:
	./genRosbag ~/Downloads/seq1_cloudy1 /laserscan /odometry


